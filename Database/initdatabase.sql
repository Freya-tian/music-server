CREATE DATABASE  IF NOT EXISTS `musicapp_schema` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `musicapp_schema`;
-- MySQL dump 10.13  Distrib 8.0.36, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: musicapp_schema
-- ------------------------------------------------------
-- Server version	8.0.36

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `albums`
--

DROP TABLE IF EXISTS `albums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `albums` (
  `Albums_ID` int NOT NULL AUTO_INCREMENT,
  `Album_Name` varchar(45) DEFAULT NULL,
  `Music_ID` int DEFAULT NULL,
  `Singer_ID` int DEFAULT NULL,
  `time_publish` date DEFAULT NULL,
  PRIMARY KEY (`Albums_ID`),
  KEY `FK_MusicID_idx` (`Music_ID`),
  KEY `FK_SingerID_idx` (`Singer_ID`),
  CONSTRAINT `FK_MusicID` FOREIGN KEY (`Music_ID`) REFERENCES `music` (`Music_ID`),
  CONSTRAINT `FK_SingerID` FOREIGN KEY (`Singer_ID`) REFERENCES `singer` (`Singer_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `albums`
--

LOCK TABLES `albums` WRITE;
/*!40000 ALTER TABLE `albums` DISABLE KEYS */;
/*!40000 ALTER TABLE `albums` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment_music`
--

DROP TABLE IF EXISTS `comment_music`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `comment_music` (
  `Comment_id` int NOT NULL AUTO_INCREMENT,
  `Music_id` int DEFAULT NULL,
  `text_comment` varchar(255) DEFAULT NULL,
  `time_publish` date DEFAULT NULL,
  `Publish_user_id` int DEFAULT NULL,
  PRIMARY KEY (`Comment_id`),
  KEY `FK_comment_music_id_idx` (`Music_id`),
  KEY `FK_publish_user_id_idx` (`Publish_user_id`),
  CONSTRAINT `FK_comment_music_id` FOREIGN KEY (`Music_id`) REFERENCES `music` (`Music_ID`),
  CONSTRAINT `FK_publish_user_id` FOREIGN KEY (`Publish_user_id`) REFERENCES `users` (`User_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment_music`
--

LOCK TABLES `comment_music` WRITE;
/*!40000 ALTER TABLE `comment_music` DISABLE KEYS */;
INSERT INTO `comment_music` VALUES (1,1,'太好听了，哥哥太棒了','2024-03-25',1);
/*!40000 ALTER TABLE `comment_music` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment_reply`
--

DROP TABLE IF EXISTS `comment_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `comment_reply` (
  `id` int NOT NULL AUTO_INCREMENT,
  `comment_id` int DEFAULT NULL,
  `publish_reply_user_id` int DEFAULT NULL,
  `publish_time` date DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `reply_type` varchar(45) DEFAULT NULL COMMENT '评论回复时的回复的id',
  `to_user_id` int DEFAULT NULL COMMENT '回复谁',
  `reply_id` int DEFAULT NULL,
  `comment_replycol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_comment_id_idx` (`comment_id`),
  KEY `FK_reply_user_id_idx` (`publish_reply_user_id`),
  KEY `FK_reply_replyID` (`reply_type`),
  KEY `FK_to_user_id_idx` (`to_user_id`),
  KEY `FK_reply_id_idx` (`reply_id`),
  CONSTRAINT `FK_comment_id` FOREIGN KEY (`comment_id`) REFERENCES `comment_music` (`Comment_id`),
  CONSTRAINT `FK_reply_id` FOREIGN KEY (`reply_id`) REFERENCES `comment_reply` (`id`),
  CONSTRAINT `FK_reply_user_id` FOREIGN KEY (`publish_reply_user_id`) REFERENCES `users` (`User_ID`),
  CONSTRAINT `FK_to_user_id` FOREIGN KEY (`to_user_id`) REFERENCES `users` (`User_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment_reply`
--

LOCK TABLES `comment_reply` WRITE;
/*!40000 ALTER TABLE `comment_reply` DISABLE KEYS */;
INSERT INTO `comment_reply` VALUES (1,1,2,'2024-03-25','是的，他太棒了呢','0',1,NULL,NULL),(2,1,3,'2024-03-25','yesyesyes','1',2,1,NULL);
/*!40000 ALTER TABLE `comment_reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `music`
--

DROP TABLE IF EXISTS `music`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `music` (
  `Music_ID` int NOT NULL AUTO_INCREMENT,
  `Title` longtext,
  `Singer_ID` int NOT NULL,
  `Lyric` varchar(100) DEFAULT NULL,
  `Source` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Music_ID`),
  KEY `FK_SingerID_idx` (`Singer_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `music`
--

LOCK TABLES `music` WRITE;
/*!40000 ALTER TABLE `music` DISABLE KEYS */;
INSERT INTO `music` VALUES (1,'闇の花嫁',2,'','assets\\Grimoire ensemble\\闇の花嫁\\闇の花嫁.mp3'),(2,'Updown',3,'','assets\\Stand by Me\\Updown\\Updown.mp3'),(3,'loveStory',4,'','assets\\Taylor Swift\\loveStory\\loveStory.mp3'),(4,'倔强',5,'assets\\五月天\\倔强\\倔强-五月天.lrc','assets\\五月天\\倔强\\倔强.mp3'),(5,'温柔',5,'assets\\五月天\\温柔\\温柔-五月天.lrc','assets\\五月天\\温柔\\温柔.mp3'),(6,'爱情的模样',5,'assets\\五月天\\爱情的模样\\爱情的模样+(Live)-五月天.lrc','assets\\五月天\\爱情的模样\\爱情的模样.mp3'),(7,'疼爱',5,'assets\\五月天\\疼爱\\疼爱+(Life+Live)-五月天,萧敬腾.lrc','assets\\五月天\\疼爱\\疼爱.mp3'),(8,'私奔到月球',5,'assets\\五月天\\私奔到月球\\私奔到月球(Qq+炫舞+228233)-五月天.lrc','assets\\五月天\\私奔到月球\\私奔到月球.mp3'),(9,'枕着光的他',6,'assets\\任素汐\\枕着光的他\\枕着光的她-任素汐.lrc','assets\\任素汐\\枕着光的他\\枕着光的他.mp3'),(10,'西海情歌',7,'','assets\\刀郎\\西海情歌\\西海情歌.mp3'),(11,'唯一',8,'','assets\\告五人\\唯一\\唯一.mp3'),(12,'兰亭序',9,'','assets\\周杰伦\\兰亭序\\兰亭序.mp3'),(13,'晴天',9,'assets\\周杰伦\\晴天\\晴天-周杰伦.lrc','assets\\周杰伦\\晴天\\晴天.mp3'),(14,'暗号',9,'','assets\\周杰伦\\暗号\\暗号.mp3'),(15,'珊瑚海',9,'','assets\\周杰伦\\珊瑚海\\珊瑚海.mp3'),(16,'稻香',9,'assets\\周杰伦\\稻香\\稻香-周杰伦.lrc','assets\\周杰伦\\稻香\\稻香.mp3'),(17,'无名的人',10,'','assets\\毛不易\\无名的人\\无名的人.mp3'),(18,'天外来物',11,'','assets\\薛之谦\\天外来物\\天外来物.mp3'),(19,'富士山下',12,'','assets\\陈奕迅\\富士山下\\富士山下.mp3'),(20,'偏偏喜欢你',13,'assets\\陈百强\\偏偏喜欢你\\偏偏喜欢你-陈百强.lrc','assets\\陈百强\\偏偏喜欢你\\偏偏喜欢你.mp3');
/*!40000 ALTER TABLE `music` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `music_tag`
--

DROP TABLE IF EXISTS `music_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `music_tag` (
  `Tag_ID` int NOT NULL AUTO_INCREMENT,
  `Tag_Name` varchar(45) DEFAULT NULL,
  `group` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Tag_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `music_tag`
--

LOCK TABLES `music_tag` WRITE;
/*!40000 ALTER TABLE `music_tag` DISABLE KEYS */;
INSERT INTO `music_tag` VALUES (1,'ballad','style music'),(2,'english','langues music'),(3,'Chinese','langues music'),(4,'sad','mood'),(5,'rock','style music'),(6,'hippop','style music'),(7,'slience','mood'),(8,'happy','mood'),(9,'DJ','style music'),(10,'classical','style music'),(11,'love song','topic');
/*!40000 ALTER TABLE `music_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `musiclist`
--

DROP TABLE IF EXISTS `musiclist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `musiclist` (
  `MusicList_ID` int NOT NULL AUTO_INCREMENT,
  `list_Name` varchar(50) DEFAULT NULL,
  `User_ID` int DEFAULT NULL,
  `Introduction` varchar(255) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  `Play_times` int DEFAULT '0',
  `Recommended` tinyint DEFAULT '0',
  PRIMARY KEY (`MusicList_ID`),
  KEY `Fk_user_Id_idx` (`User_ID`),
  CONSTRAINT `FK_musiclist_userid` FOREIGN KEY (`User_ID`) REFERENCES `users` (`User_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `musiclist`
--

LOCK TABLES `musiclist` WRITE;
/*!40000 ALTER TABLE `musiclist` DISABLE KEYS */;
INSERT INTO `musiclist` VALUES (1,'my favorite music',1,NULL,'/assest/album.jpg',10,1),(2,'english songs',1,NULL,NULL,0,0),(3,'温柔华语 | 把故事藏在歌中',1,'好听的歌，就是要循环！循环！再循环！这些耐听好歌送给大家，真的会越听越爱喔',NULL,146,1),(4,'音乐里那些不言而喻的“我爱你”',1,NULL,NULL,55,1);
/*!40000 ALTER TABLE `musiclist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `musiclist_music`
--

DROP TABLE IF EXISTS `musiclist_music`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `musiclist_music` (
  `Music_ID` int NOT NULL,
  `MusicList_ID` int NOT NULL,
  PRIMARY KEY (`Music_ID`,`MusicList_ID`),
  KEY `FK_MusicList_ID_idx` (`MusicList_ID`),
  CONSTRAINT `FK_Music_ID` FOREIGN KEY (`Music_ID`) REFERENCES `music` (`Music_ID`),
  CONSTRAINT `FK_MusicList_ID` FOREIGN KEY (`MusicList_ID`) REFERENCES `musiclist` (`MusicList_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `musiclist_music`
--

LOCK TABLES `musiclist_music` WRITE;
/*!40000 ALTER TABLE `musiclist_music` DISABLE KEYS */;
INSERT INTO `musiclist_music` VALUES (5,1),(6,1),(8,1),(10,1),(15,1),(20,1),(2,2),(3,2),(4,2);
/*!40000 ALTER TABLE `musiclist_music` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `musiclist_tags`
--

DROP TABLE IF EXISTS `musiclist_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `musiclist_tags` (
  `MusicList_ID` int NOT NULL,
  `Tag_ID` int NOT NULL,
  PRIMARY KEY (`MusicList_ID`,`Tag_ID`),
  KEY `FK_Tag_ID_idx` (`Tag_ID`),
  CONSTRAINT `FK_MusicListID` FOREIGN KEY (`MusicList_ID`) REFERENCES `musiclist` (`MusicList_ID`),
  CONSTRAINT `FK_Tag_ID` FOREIGN KEY (`Tag_ID`) REFERENCES `music_tag` (`Tag_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `musiclist_tags`
--

LOCK TABLES `musiclist_tags` WRITE;
/*!40000 ALTER TABLE `musiclist_tags` DISABLE KEYS */;
INSERT INTO `musiclist_tags` VALUES (1,2),(1,3),(1,5),(2,6),(3,7),(3,10),(3,11),(4,11);
/*!40000 ALTER TABLE `musiclist_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rank_list`
--

DROP TABLE IF EXISTS `rank_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rank_list` (
  `Rank_ID` int NOT NULL AUTO_INCREMENT,
  `Music_ID` int DEFAULT NULL,
  `rank` int DEFAULT NULL,
  PRIMARY KEY (`Rank_ID`),
  KEY `FK_Music_idx` (`Music_ID`),
  CONSTRAINT `FK_Music` FOREIGN KEY (`Music_ID`) REFERENCES `music` (`Music_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rank_list`
--

LOCK TABLES `rank_list` WRITE;
/*!40000 ALTER TABLE `rank_list` DISABLE KEYS */;
INSERT INTO `rank_list` VALUES (1,5,1),(2,6,2),(3,10,3),(4,11,5),(5,8,4),(6,3,6),(7,20,7),(8,15,8),(9,16,9),(10,17,10);
/*!40000 ALTER TABLE `rank_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reply_type`
--

DROP TABLE IF EXISTS `reply_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reply_type` (
  `reply_type` varchar(45) NOT NULL DEFAULT 'comment',
  `reply_id` int DEFAULT NULL,
  `comment_id` int DEFAULT NULL,
  KEY `FK_reply_id_idx` (`reply_id`),
  KEY `FK_comment_id_idx` (`comment_id`),
  CONSTRAINT `FK_reply_comment_id` FOREIGN KEY (`comment_id`) REFERENCES `comment_music` (`Comment_id`),
  CONSTRAINT `FK_reply_reply_id` FOREIGN KEY (`reply_id`) REFERENCES `comment_reply` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reply_type`
--

LOCK TABLES `reply_type` WRITE;
/*!40000 ALTER TABLE `reply_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `reply_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `singer`
--

DROP TABLE IF EXISTS `singer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `singer` (
  `Singer_ID` int NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) DEFAULT NULL,
  `Nationality` varchar(45) DEFAULT NULL,
  `Profession` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Singer_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `singer`
--

LOCK TABLES `singer` WRITE;
/*!40000 ALTER TABLE `singer` DISABLE KEYS */;
INSERT INTO `singer` VALUES (1,'Charlie Puth',NULL,NULL,'assets\\Charlie Puth\\Charlie Puth.PNG'),(2,'Grimoire ensemble',NULL,NULL,''),(3,'Stand by Me',NULL,NULL,''),(4,'Taylor Swift',NULL,NULL,'assets\\Taylor Swift\\Taylor Swift.PNG'),(5,'五月天',NULL,NULL,''),(6,'任素汐',NULL,NULL,'assets\\任素汐\\任素汐.PNG'),(7,'刀郎',NULL,NULL,'assets\\刀郎\\刀郎.PNG'),(8,'告五人',NULL,NULL,'assets\\告五人\\告五人.PNG'),(9,'周杰伦',NULL,NULL,'assets\\周杰伦\\周杰伦.PNG'),(10,'毛不易',NULL,NULL,'assets\\毛不易\\毛不易.PNG'),(11,'薛之谦',NULL,NULL,'assets\\薛之谦\\薛之谦.PNG'),(12,'陈奕迅',NULL,NULL,'assets\\陈奕迅\\陈奕迅.PNG'),(13,'陈百强',NULL,NULL,'assets\\陈百强\\陈百强.PNG');
/*!40000 ALTER TABLE `singer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `User_ID` int NOT NULL AUTO_INCREMENT,
  `User_Name` varchar(45) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `Email` varchar(45) DEFAULT NULL,
  `Avatar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`User_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'tianqi','e10adc3949ba59abbe56e057f20f883e','user@example.com','data\\Avatar\\avatar_b627c646-a1d0-4ac7-b412-3f5e6448e130.png'),(2,'freya','123456','text@uaer.com','xxxx'),(3,'111','123456','user2@user.com','111');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'musicapp_schema'
--

--
-- Dumping routines for database 'musicapp_schema'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-04-04  5:07:45
